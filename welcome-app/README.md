# Welcome Application
HTTP server which displays a welcome message in the browser.

Run the application by changing to the _welcom
```
cd welcome-app
go run main.go
```
Once running, go to http://localhost:8080/?name=MINI.

Based upon the tutorial:
 https://medium.com/google-cloud/building-a-go-web-app-from-scratch-to-deploying-on-google-cloud-part-1-building-a-simple-go-aee452a2e654
