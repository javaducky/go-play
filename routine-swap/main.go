package main

import (
	"fmt"
	"sync"
)

// wg is used to wait for the program to finish
var wg sync.WaitGroup

// main is the entry point for all Go programs.
func main() {
	// Allocate 1 logical processor for the scheduler (DEMO PURPOSE ONLY!!!!)
	// runtime.GOMAXPROCS(1)

	// Add count for each routine
	wg.Add(2)

	// Create routines
	fmt.Println("Create Goroutines")
	go printPrime("A")
	go printPrime("B")

	// Wait for the routines to finish
	fmt.Println("Waiting to finish...")
	wg.Wait()

	fmt.Println("Terminating program")
}

// printPrim displays prime numbers for the first 5000 numbers.
func printPrime(prefix string) {
	// Schedule the call to Done to tell main we're done.
	defer wg.Done()

next:
	for outer := 2; outer < 5000; outer++ {
		for inner := 2; inner < outer; inner++ {
			if outer%inner == 0 {
				continue next
			}
		}
		fmt.Printf("%s:%d\n", prefix, outer)
	}
	fmt.Println("Completed ", prefix)
}
