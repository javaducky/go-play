# CLI Example
Shows basic usage for creating a command-line application.

Install and run the application:
```
cd cli-example
go install

cli-example --user bobo,pablo
```

The application should display that it is "searching users" and echo the user names.

Once complete, uninstall the application using:
```
cd cli-example
go clean -i
```

Based upon the tutorial:
https://medium.freecodecamp.org/writing-command-line-applications-in-go-2bc8c0ace79d
